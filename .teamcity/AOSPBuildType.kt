import jetbrains.buildServer.configs.kotlin.v2019_2.BuildType
import jetbrains.buildServer.configs.kotlin.v2019_2.CheckoutMode
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.script
import jetbrains.buildServer.configs.kotlin.v2019_2.vcs.GitVcsRoot

open class AOSPBuildType(
        private val idParam: String,
        private val nameParam: String,
        private val vcsRoots: List<Pair<GitVcsRoot, String>>
) : BuildType({
    id(idParam)
    name = nameParam

    vcs {
        checkoutMode = CheckoutMode.ON_AGENT

        vcsRoots.forEach {
            root(it.first, it.second)
        }
    }

    steps {
        // TODO: parse commands from manifest
        script {
            name = "copy"
            scriptContent = "cp \"build/core/root.mk\" \"build/Makefile\""
        }

        script {
            name = "ls"
            scriptContent = "ls -l"
        }
    }
})