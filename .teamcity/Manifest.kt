import org.w3c.dom.Node
import java.io.File
import javax.xml.parsers.DocumentBuilderFactory

private data class RepoRemote(val name: String, val fetch: String)
private data class RepoDefault(val revision: String, val remote: String)

data class RepoProject(val path: String, val name: String, val revision: String, val fetch: String) {
    val id: String
        get() = uniqueName
                .replace("/", "_")
                .replace("-", "_")
                .replace(".", "_")

    val uniqueName: String
        get() = "${name}_${revision}"
}

fun parseManifest(path: String): List<RepoProject> {
    val manifestFile = File(path)

    if (!manifestFile.exists()) {
        throw RuntimeException("Failed to parse manifest: ${manifestFile.absoluteFile} not found!")
    }

    val documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
    val document = documentBuilder.parse(manifestFile)

    if (document.documentElement.tagName != MANIFEST_ROOT_NAME) {
        throw RuntimeException("Repo manifest must have <manifest> as root tag. Actual: ${document.documentElement.tagName}")
    }

    val elements: List<Node> = document.documentElement.childNodesList()

    val remotes: Map<String, RepoRemote> = elements.getRemotes()
    val default: RepoDefault = elements.getDefault()!!

    return elements
            .filter { it.nodeName == MANIFEST_PROJECT_NAME }
            .map {
                val name = it.attributes.getNamedItem(MANIFEST_PROJECT_NAME_ATTRIBUTE).nodeValue
                if (name == null || name.isBlank()) {
                    throw RuntimeException("Project tag must have name attribute. Actual: $name")
                }

                val path = it.attributes.getNamedItem(MANIFEST_PROJECT_PATH_ATTRIBUTE).nodeValue
                if (path == null || path.isBlank()) {
                    throw RuntimeException("Project tag must have path attribute. Actual: $path")
                }

                val remote = remotes[default.remote]
                        ?: throw RuntimeException("Remote: ${default.remote} specified in default not found")

                RepoProject(path, name, default.revision, remote.fetch)
            }
}

private fun List<Node>.getRemotes(): Map<String, RepoRemote> = this
        .filter { it.nodeName == MANIFEST_REMOTE_NAME }
        .map {
            val name = it.attributes.getNamedItem(MANIFEST_REMOTE_NAME_ATTRIBUTE).nodeValue
            if (name == null || name.isBlank()) {
                throw RuntimeException("Remote tag must have name attribute. Actual: $name")
            }

            val fetch = it.attributes.getNamedItem(MANIFEST_REMOTE_FETCH_ATTRIBUTE).nodeValue
            if (fetch == null || fetch.isBlank()) {
                throw RuntimeException("Fetch tag must have name attribute. Actual: $fetch")
            }

            name to RepoRemote(name = name, fetch = fetch)
        }
        .toMap()

private fun List<Node>.getDefault(): RepoDefault? {
    val defaults: List<RepoDefault> = this
            .filter { it.nodeName == MANIFEST_DEFAULT_NAME }
            .map {
                val revision = it.attributes.getNamedItem(MANIFEST_DEFAULT_REVISION_ATTRIBUTE).nodeValue
                if (revision == null || revision.isBlank()) {
                    throw RuntimeException("Revision tag must have name attribute. Actual: $revision")
                }

                val remote = it.attributes.getNamedItem(MANIFEST_DEFAULT_REMOTE_ATTRIBUTE).nodeValue
                if (remote == null || remote.isBlank()) {
                    throw RuntimeException("Remote tag must have name attribute. Actual: $remote")
                }

                RepoDefault(revision = revision, remote = remote)
            }

    if (defaults.size > 1) {
        throw RuntimeException("Default must be only one. Actual amount: ${defaults.size}")
    }

    return defaults.firstOrNull()
}

private fun Node.childNodesList(): List<Node> = (0 until childNodes.length).map { childNodes.item(it) }

private const val MANIFEST_REMOTE_NAME = "remote"
private const val MANIFEST_REMOTE_NAME_ATTRIBUTE = "name"
private const val MANIFEST_REMOTE_FETCH_ATTRIBUTE = "fetch"

private const val MANIFEST_DEFAULT_NAME = "default"
private const val MANIFEST_DEFAULT_REVISION_ATTRIBUTE = "revision"
private const val MANIFEST_DEFAULT_REMOTE_ATTRIBUTE = "remote"

private const val MANIFEST_PROJECT_NAME = "project"
private const val MANIFEST_PROJECT_PATH_ATTRIBUTE = "path"
private const val MANIFEST_PROJECT_NAME_ATTRIBUTE = "name"

private const val MANIFEST_ROOT_NAME = "manifest"
