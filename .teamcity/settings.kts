import jetbrains.buildServer.configs.kotlin.v2019_2.project
import jetbrains.buildServer.configs.kotlin.v2019_2.vcs.GitVcsRoot
import jetbrains.buildServer.configs.kotlin.v2019_2.version
import java.util.concurrent.TimeUnit

version = "2019.2"

val android6VcsRoots: List<Pair<GitVcsRoot, String>> = parseManifest("manifest/android6.xml").map { it.toVcsRoot() }
val android4VcsRoots: List<Pair<GitVcsRoot, String>> = parseManifest("manifest/android4.xml").map { it.toVcsRoot() }
val android5VcsRoots: List<Pair<GitVcsRoot, String>> = parseManifest("manifest/android5.xml").map { it.toVcsRoot() }

project {

    (android4VcsRoots + android5VcsRoots + android6VcsRoots).forEach {
        vcsRoot(it.first)
    }

    buildType(AOSPBuildType(idParam = "android4", nameParam = "Android4", vcsRoots = android4VcsRoots))
    buildType(AOSPBuildType(idParam = "android5", nameParam = "Android5", vcsRoots = android5VcsRoots))
    buildType(AOSPBuildType(idParam = "android6", nameParam = "Android6", vcsRoots = android6VcsRoots))
}

fun RepoProject.toVcsRoot(): Pair<GitVcsRoot, String> = GitVcsRoot {
    id(this@toVcsRoot.id)
    name = this@toVcsRoot.uniqueName
    url = this@toVcsRoot.fetch + "/" + this@toVcsRoot.name
    branch = this@toVcsRoot.revision

    // Disabling polling every repository in concern for rate limits and teamcity performance:
    // https://youtrack.jetbrains.com/issue/TW-4134
    this.pollInterval = TimeUnit.DAYS.toSeconds(365 * 3).toInt()

    useTagsAsBranches = true
} to "+:. => $path"